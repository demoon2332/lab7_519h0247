import 'package:flutter_test/flutter_test.dart';

import 'package:bignumber_util/bignumber_util.dart';

void main() {
  test('adds one to input values', () {
    // final calculator = Calculator();
    // expect(calculator.addOne(2), 3);
    // expect(calculator.addOne(-7), -6);
    // expect(calculator.addOne(0), 1);

    final calculator = BigNumber_Util();

    expect(calculator.sum("1234","892"),"2126");
    expect(calculator.sum("1234","234"),"1468");
    expect(calculator.sum("8392651","4254290"),"12646941");
    expect(calculator.sum("631374304","377354731"),"1008729035");
  });
}
