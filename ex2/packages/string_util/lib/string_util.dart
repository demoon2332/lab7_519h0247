library string_util;

class StringUtil{
  String removeRedundantSpace(String str){
    return str.trim().replaceAll(RegExp(' +'), ' ');
  }

  int countWords(String str){
    str = removeRedundantSpace(str);
    List<String> words = str.split(' ');
    return words.length;
  }

  String removeAccent(String str){
    var accented = 'ÀÁÃẢẠĂẰẮẲẴẶÂẦẤẨẪẬÈÉẺẼẸÊỀẾỂỄỆĐÙÚỦŨỤƯỪỨỬỮỰÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÌÍỈĨỊÄËÏÎÖÜÛÑÇÝỲỸỴỶàáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ';
    var unaccented = 'AAAAAAAAAAAAAAAAAEEEEEEEEEEEDUUUUUUUUUUUOOOOOOOOOOOOOOOOOIIIIIAEIIOUUNCYYYYYaaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy';

    for(int i=0;i<accented.length;i++){
      str = str.replaceAll(accented[i],unaccented[i]);
    }
    return str;
  }

}
