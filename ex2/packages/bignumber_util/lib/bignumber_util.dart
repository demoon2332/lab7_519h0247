library bignumber_util;

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}

class BigNumber_Util{
  String sum(String a,String b){
    String result = "";
    if(a.length > b.length){
      while(a.length > b.length){
        b = "0"+b;
      }
    }
    else if(b.length > a.length){
      while(b.length > a.length){
        a = "0" + a;
      }
    }
    // only when a.length == b.length, we get into calculating
      int temp = 0;
      for(int i=a.length-1;i>=0;i--){
          int c = int.parse(a[i])+int.parse(b[i])+ temp;
          if(c > 9){
            temp = 1;
          }
          else{
            temp =0;
          }
          if(i != 0){
            c = c % 10;
          }
          result = c.toString() + result;
      }
    return result;
  }
}