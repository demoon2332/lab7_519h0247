


import 'package:ex2/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:string_util/string_util.dart';

class RemoveAccent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
     return _RemoveAccentState();
  }

}

class _RemoveAccentState extends State<StatefulWidget>{
  String result = "";
  final _formKey = GlobalKey<FormState>();
  final stringController = TextEditingController();

  @override
  void dispose(){
    stringController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Remove Accent Package'),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(32),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: stringController,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.add),
                            border: UnderlineInputBorder(),
                            labelText: 'Your String',
                            hintText: 'Enter a string',
                          ),
                          autocorrect: false,
                          autofocus: true,
                          textCapitalization: TextCapitalization.none,
                          validator: (value){
                            if(value ==  null || value.isEmpty){
                              return 'Please enter a string.';
                            }
                            return null;
                          },
                        )
                    )
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(height: 15),
                    Expanded(
                      child: Text(
                        'Result: '+ result,
                        style: const TextStyle(fontSize: 30),
                      )
                    ),
                  ],
                ),
                TextButton(
                    onPressed: (){
                      if(_formKey.currentState!.validate()){
                        remove(stringController.text);
                      }
                    },
                    child: const Text('Remove Accent'))
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => const MyApp()));
        },
        tooltip: 'Back to Home',
        child: const Icon(Icons.house_sharp),
      ),
    );
  }


  void remove(String str){
    StringUtil engine = StringUtil();
    setState(() {
      result = engine.removeAccent("str");
    });
  }

}