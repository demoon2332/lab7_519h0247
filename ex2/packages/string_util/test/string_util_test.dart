import 'package:flutter_test/flutter_test.dart';

import 'package:string_util/string_util.dart';

void main() {
  test('adds one to input values', () {
    final handler = StringUtil();

    expect(handler.removeAccent("Lớp 1234"), "Lop 1234");
    expect(handler.removeAccent("Đại Học Tôn Đức Thắng"), "Dai Hoc Ton Duc Thang");
    expect(handler.removeAccent("Cloé"), "Cloe");
    expect(handler.removeAccent("Nguyễn Đức Trọng"), "Nguyen Duc Trong");
  }
  );
}
