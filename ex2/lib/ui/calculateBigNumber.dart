


import 'package:bignumber_util/bignumber_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class CalculateBigNumber extends StatefulWidget {
  const CalculateBigNumber({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CalculateBigNumber();
  }

}

class _CalculateBigNumber extends State<StatefulWidget>{
  String result = "";
  final _formKey = GlobalKey<FormState>();
  final numberAController = TextEditingController();
  final numberBController = TextEditingController();

  @override
  void dispose(){
    numberAController.dispose();
    numberBController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Remove Accent Package'),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(32),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: numberAController,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Number A',
                            hintText: 'Enter a number',
                          ),
                          autocorrect: true,
                          autofocus: false,
                          textCapitalization: TextCapitalization.none,
                          keyboardType: const TextInputType.numberWithOptions(signed: false,decimal: false),
                          validator: (value){
                            if(value ==  null || value.isEmpty){
                              return 'Please enter a number to calculate.';
                            }
                            else if(double.tryParse(value) == null){
                              return 'Please enter a number.';
                            }
                            return null;
                          },
                        )
                    )
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: numberBController,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Number B',
                            hintText: 'Enter a number',
                          ),
                          autocorrect: false,
                          autofocus: false,
                          textCapitalization: TextCapitalization.none,
                          keyboardType: const TextInputType.numberWithOptions(signed: false,decimal: false),
                          validator: (value){
                            if(value ==  null || value.isEmpty){
                              return 'Please enter a number to calculate.';
                            }
                            else if(double.tryParse(value) == null){
                              return 'Please enter a number.';
                            }
                            return null;
                          },
                        )
                    )
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(height: 15),
                    Expanded(
                        child: Text(
                          'Result: '+ result,
                          style: const TextStyle(fontSize: 30),
                        )
                    ),
                  ],
                ),
                TextButton(
                    onPressed: (){
                      if(_formKey.currentState!.validate()){
                        sum(numberAController.text,numberBController.text);
                      }
                    },
                    child: const Text('Sum'))
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => const MyApp()));
        },
        tooltip: 'Back to Home',
        child: const Icon(Icons.house_sharp),
      ),
    );
  }

  void sum(String a,String b){
    BigNumber_Util calculator = BigNumber_Util();
    setState(() {
      result = calculator.sum(a,b);
    });
  }

}